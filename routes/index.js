'use strict';

var express = require('express');
var router = express.Router();
var fs = require('fs');
var imgSize = require('image-size');
var mongo = require('mongoose');

// データベースに接続。
var db = mongo.connect('mongodb://localhost/roomDB');
// スキーマを宣言
var roomSchema = new mongo.Schema({
	userId: String,
	password: String,
	userName: String,
	roomName: String,
	height: Number,
	wallInfo: Array,
	sceneList: Object
});
//スキーマからモデルを生成。
var Room = db.model('room',roomSchema);

/* GET INDEX page. */
router.get('/', function(req, res) {
	res.render('index');
});

/* GET EDIT page. */
router.get('/edit', function(req, res) {
	res.render('edit');
});

/* GET EDIT page. */
router.get('/creatRoom', function(req, res) {
	res.render('creatRoom');
});

/* get getRoom data. */
router.get('/getRoomData', function(req, res) {

	Room.find(req.query, function(err, items){
		if(err){console.log(err); return;}

		res.send(items);
	});
});

/* get setRoom data. */
router.get('/setRoomData', function(req, res) {
	//モデルからインスタンス作成
	var room = new Room(req.query);
	// 削除
	Room.remove({}, function(){console.log('削除できた')});

	//データベースに保存
	room.save(function(err){
		Room.find(function(err, items){
			if(err){console.log(err); return;}

			console.log(items);
		});
	});

	res.redirect('/edit');
});

/* POST image file. */
router.post('/upload', function(req, res) {
	var imagePath = '/uploads/' + req.files[Object.keys(req.files)[0]].name;
	var dimensions = imgSize('public/' + imagePath);

	res.send({
		fileDir: imagePath,
		width: dimensions.width,
		height: dimensions.height,
		name: req.files[Object.keys(req.files)[0]].originalname
	});
});

/* POST image file. */
router.post('/textureUpload', function(req, res) {
	var imagePath = '/uploads/' + req.files[Object.keys(req.files)[0]].name;
	var dimensions = imgSize('public/' + imagePath);

	res.send({
		fileDir: imagePath,
		width: dimensions.width,
		height: dimensions.height,
		name: req.files[Object.keys(req.files)[0]].originalname
	});
});

module.exports = router;
