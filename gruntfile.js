module.exports = function(grunt) {
	//グラントタスクの設定
	grunt.initConfig({
		browserify: {
			dist: {
				files: {
				  'public/js/top.js': ['develop/js/top.js'],
				  'public/js/edit.js': ['develop/js/edit.js'],
				  'public/js/creatRoom.js': ['develop/js/creatRoom.js'],
				  'public/js/roomList.js': ['develop/js/roomList.js']
				},
				options: {
					transform: ['brfs']
				}
			}
		},
		//sassの設定
		sass: {
			dist: {
				files: {
					'public/css/top.css': 'develop/scss/top.scss',
					'public/css/edit.css': 'develop/scss/edit.scss',
					'public/css/creatRoom.css': 'develop/scss/creatRoom.scss',
					'public/css/roomList.css': 'develop/scss/roomList.scss'
				}
			}
		},
		//watchの設定
		watch: {
			css: {
				files: ['develop/scss/*.scss'],
				tasks: ['sass']
			},
			js: {
				files: ['develop/js/*.js'],
				tasks: ['browserify']
			},
		}
	});
	//プラグインの読み込み
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-browserify');
};
