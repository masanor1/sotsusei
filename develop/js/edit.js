// MY ROOM

'use strict';

var $ = require('jquery');
var _ = require('underscore');

var creatRoom = require('./room');

var $win = $(window);
var $doc = $(document);
var $field = $('#field');
var $header = $('#js-header');
var $3dTextInput = $('#js-3dText');
var $managerView = $('#js-objectManager');
var $textureWindow = $('#js-textureWindow');
var WORK_IMAGE_PATH = '/upload';
var TEXTURE_IMAGE_PATH = '/textureUpload';
var room;
var width = $field.width();
var height = $field.height();

var loading = {
	// 部屋、光で2
	state: 3,

	load: function() {
		var self = this;

		if (self.state === 0) {
			setTimeout(function() {
				$('#js-loading').find('.js-loadingGIF').fadeOut(500, function() {
					$('#js-loading').fadeOut(500);
				});
			}, 2000);

			return;
		}

		setTimeout(function() {
			self.load();
		}, 250);
	}
};

loading.load();

/* オブジェクトマネージャ */
var objectManager = {
	$managerView: $managerView,

	objectList: {},

	selected: null,

	line: {
		material: null,
		geometry: null,
		mesh: null
	},

	// lineX: {
	// 	material: null,
	// 	geometry: null,
	// 	mesh: null
	// },
	// lineY: {
	// 	material: null,
	// 	geometry: null,
	// 	mesh: null
	// },
	// lineZ: {
	// 	material: null,
	// 	geometry: null,
	// 	mesh: null
	// },

	ObjListTamplate: _.template('<li class="objectInfo js-infoList"><a href="javascript:void(0)" class="hide fl"></a><div class="name fl js-infoName"><%= name %></div></li>'),

	add: function(objectInfo) {
		var objectInfoClone = objectInfo;
		var originalName = objectInfo.name
		var i = 2;

		while(!!this.objectList[objectInfoClone.name]) {
			objectInfoClone.name = originalName + i;
			i++;
		}

		this.objectList[objectInfoClone.name] = objectInfoClone;

		this.addHTML(objectInfoClone);
	},

	addHTML: function(objectInfo) {
		this.$managerView.find('.js-objectList').append(this.ObjListTamplate(objectInfo));
	},

	getObject: function(name) {
		return this.objectList[name].object;
	},

	selectObject: function(name) {
		var object = this.getObject(name);
		// var xLinePositon = {
		// 	near: _.clone(object.position),
		// 	far: _.clone(object.position)
		// };
		// var yLinePositon = {
		// 	near: _.clone(object.position),
		// 	far: _.clone(object.position)
		// };
		// var zLinePositon = {
		// 	near: _.clone(object.position),
		// 	far: _.clone(object.position)
		// };

		// xLinePositon.near.x -= 10;
		// xLinePositon.far.x += 10;
		// yLinePositon.near.y -= 10;
		// yLinePositon.far.y += 10;
		// zLinePositon.near.z -= 10;
		// zLinePositon.far.z += 10;

		this.selected = object;

		this.line.material = new THREE.LineBasicMaterial({color: 0x000000, linewidth: 5});
		// this.lineX.material = new THREE.LineBasicMaterial({color: 0xff0000, linewidth: 3});
		// this.lineY.material = new THREE.LineBasicMaterial({color: 0x00ff00, linewidth: 3});
		// this.lineZ.material = new THREE.LineBasicMaterial({color: 0x0000ff, linewidth: 3});

		this.line.geometry = new THREE.Geometry();
		// this.lineX.geometry = new THREE.Geometry();
		// this.lineY.geometry = new THREE.Geometry();
		// this.lineZ.geometry = new THREE.Geometry();

		this.line.geometry.vertices.push(new THREE.Vector3(0, 500, 0), object.position);
		// this.lineX.geometry.vertices.push(xLinePositon.near, xLinePositon.far);
		// this.lineY.geometry.vertices.push(yLinePositon.near, yLinePositon.far);
		// this.lineZ.geometry.vertices.push(zLinePositon.near, zLinePositon.far);

		this.line.mesh = new THREE.Line(this.line.geometry, this.line.material);
		// this.lineX.mesh = new THREE.Line(this.lineX.geometry, this.lineX.material);
		// this.lineY.mesh = new THREE.Line(this.lineY.geometry, this.lineY.material);
		// this.lineZ.mesh = new THREE.Line(this.lineZ.geometry, this.lineZ.material);

		scene.add(this.line.mesh);
		// scene.add(this.lineX.mesh);
		// scene.add(this.lineY.mesh);
		// scene.add(this.lineZ.mesh);
	},

	unselectObject: function() {
		this.selected = null;

		scene.remove(this.line.mesh);
		// scene.remove(this.lineX.mesh);
		// scene.remove(this.lineY.mesh);
		// scene.remove(this.lineZ.mesh);
	}
};

/** シーンの生成 **/
var scene = new THREE.Scene();
/** カメラの生成 **/
/*
クォータニオンで計算するようにする
http://gupuru.hatenablog.jp/entry/2013/12/13/204721
*/
// 視野角・アスペクト比・最小描画範囲・最大描画範囲
var camera = new THREE.PerspectiveCamera(50, width / height, 1, 1000);

// x軸, y軸, z軸
camera.position.set(0, 75, 50);
// カメラの表示範囲
camera.aspect = 60 / 40;

/** ライトの作成 **/
function setLight(){
	// 空の光
	var hemiLight = new THREE.HemisphereLight(0xdddddd, 0xcccccc, 0.7); 

	// var pointLight = new THREE.PointLight (0xffffff, 1, 200);
	// pointLight.target.position.set(0, 0, 0);
	// pointLight.position.set(0, 160, 0);
	// pointLight.castShadow = true;
	// pointLight.shadowBias = -0.0001;
	// pointLight.shadowDarkness = 0.7;

	// スポットライト
	var spotLight = new THREE.SpotLight(0xffffff, 0.5, 130, Math.PI/2, 1);
	spotLight.target.position.set(0, 0, 0);
	spotLight.position.set(0, 160, 0);
	spotLight.castShadow = true;
	spotLight.shadowBias = -0.0001;
	spotLight.shadowDarkness = 0.7;
	spotLight.shadowMapHeight = spotLight.shadowMapWidth = 1500;

	// var light = new THREE.DirectionalLight(0xffffff, 0.7);
	// light.castShadow = true;
	// light.shadowDarkness = 0.5;
	// light.position.set(0, 150, 0).normalize();

	// scene.add(light);
	// scene.add(pointLight);
	scene.add(hemiLight);
	scene.add(spotLight);

	loading.state--;
}

/** 部屋の作成 **/
function setRoom(){
	var roomData;

	$.ajax({
		url: '/getRoomData',
		type: 'get',
		data: {userId: USER_ID},
		async: false
	})
	.done(function (data) {
		roomData = data[0];
		// ドーム状の部屋
		_.forEach(roomData.wallInfo, function (item, i) {
			if (typeof item === 'string') {
				var radius = Number(item);
				var material = new THREE.MeshPhongMaterial({
					color: 0xffffff,
					side: THREE.DoubleSide,
					shininess: 1,
					ambient:0xffffff
				});
				var plane = new THREE.Mesh(new THREE.PlaneGeometry(radius, radius, 40, 40), material);

				plane.rotation.x = Math.PI / 2;
				plane.position.set(0, 55, 0);

				scene.add(plane);

				plane.scale.x = 15;
				plane.scale.y = 15;
				plane.scale.z = 15;

				plane.castShadow = true;
				plane.receveShadow = true;

				room = new THREE.Mesh(new THREE.SphereGeometry(radius/2, 50, 50), material);
				room.position.set(0, 65, 0);

				return;
			}

			// 通常の形状の部屋
			_.forEach(item, function (data, j) {
				roomData.wallInfo[i][j] = JSON.parse(data);
			});
		});
	})
	.fail(function (err) {
		console.log(err);
	});

	if (!roomData) {
		alert('部屋の形を作成してください');

		return;
	}

	// 通常の形状の部屋
	if (typeof roomData.wallInfo[0] !== 'string') {
		room = creatRoom.getRoom(roomData, true);
		room.rotation.y = Math.PI / 2;
		room.position.set(0, 65, 0);
	}

	// 影をON
	room.castShadow = true;
	room.receiveShadow = true;

	scene.add(room);

	room.scale.x = 15;
	room.scale.y = 15;
	room.scale.z = 15;

	loading.state--;
}

/** 床の作成 **/
function setPlane(){
	loading.state--;
}

var room;

setRoom();
setLight();
setPlane();

/** レンダラーの生成 **/
var renderer;

// WebGLの有無を確認
if (window.WebGLRenderingContext) {
	renderer = new THREE.WebGLRenderer({ antialias: true }); 
} else {
	renderer = new THREE.CanvasRenderer({ antialias: true });
}

// 影をON
renderer.shadowMapEnabled = true; 
// 影をソフトシャドウに
renderer.shadowMapSoft = true;
renderer.shadowMapType = THREE.PCFSoftShadowMap;
// canvasサイズ設定
renderer.setSize(width, height);
// canvasサイズ設定
renderer.setClearColor(0x99aacc, 1);
// HTMLに埋め込む
$field.css({width: width, height: height}).html(renderer.domElement);
myRender();

function myRender() {
	requestAnimationFrame(myRender);
	renderer.render(scene, camera);
}

/* 画像を元に作品キャンバスを生成する */
function makeWorks(work) {
	var white = new THREE.MeshPhongMaterial({color: 0xffffff, /*transparent: true, opacity: 0,*/ shininess: 0});

	// キャンバスの縦横比
	var materials = [
		white,
		white,
		white,
		white,
		white,
		new THREE.MeshLambertMaterial({map: THREE.ImageUtils.loadTexture(work.fileDir), /*transparent: true, opacity: 0,*/ shininess: 0})
	];
	var workGeo = new THREE.BoxGeometry(
			work.width / 60,
			work.height / 60,
			1,
			30,
			30,
			30
	);
	var workMaterial = new THREE.MeshFaceMaterial(materials);

	var workObject = new THREE.Mesh(workGeo, workMaterial);
	workObject.rotation.x = 2.5;
	workObject.rotation.z = Math.PI;
	workObject.position.set(camera.position.x, 75, camera.position.z - 25);
	// 影をON
	workObject.castShadow = true;
	workObject.receiveShadow = true;

	scene.add(workObject);
	objectManager.add({
		name: work.name,
		object: workObject
	});
	// fadeIn(workObject);
}

// /* ふわっと出現させる */
// function fadeIn(workObject) {
//  if (workObject.material.materials[0].opacity <= 1) {
//    requestAnimationFrame(fadeIn);
//    _.forEach(workObject.material.materials, function(obj) {
//      obj.opacity += 0.05;
//    }); 
//  }
// }

/* 画像をサーバーに上げる */
function uploadFiles(url, files, callback) {
	var formData = new FormData(),
			xhr = new XMLHttpRequest(),
			file,
			i;

	for (i = 0, file; file = files[i]; ++i) {
		formData.append(file.name, file);
	}

	xhr.open('POST', url, true);
	xhr.onload = function() {
		// worksImage = (new Function('return ' + xhr.response))();

		callback((new Function('return ' + xhr.response))());
	};

	xhr.send(formData);
}

$('#js-makeText').on('click', function() {
	var textValue = $3dTextInput.val();
	var textSize = $('#js-3dTextSize').val();
	var textMainColor = Number($('#js-3dTextMainColor').val().replace('#', '0x'));
	var textSubColor = Number($('#js-3dTextSubColor').val().replace('#', '0x'));

	if (textValue === '') {
		textValue = $3dTextInput.attr('placeholder');
	}

	if (textSize === '') {
		textSize = 8;
	}

	//正面マテリアルの生成
	var materialFront = new THREE.MeshBasicMaterial({color: textMainColor, shininess: 0});

	//側面マテリアルの生成
	var materialSide = new THREE.MeshBasicMaterial({color: textSubColor, shininess: 0});
	var materialArray = [ materialFront, materialSide ];

	var textGeometry = new THREE.TextGeometry(
		textValue,
		{
			size: textSize, height: 4, curveSegments: 3,
			font: 'helvetiker', weight: 'bold', style: 'normal',
			bevelThickness: 1, bevelSize: 1, bevelEnabled: true,
			material: 0, extrudeMaterial: 1
		}
	);

	var textMaterial = new THREE.MeshFaceMaterial(materialArray);
	var text = new THREE.Mesh(textGeometry, textMaterial );

	// 影をON
	text.castShadow = true;
	text.receiveShadow = true;

	scene.add(text);
	objectManager.add({
		name: textValue,
		object: text
	});

	text.position.set(0, 70, 0);
});

document
	.getElementById('js-uploadWork')
	.querySelector('input[type="file"]')
	.addEventListener('change', function() {
		uploadFiles(WORK_IMAGE_PATH, this.files, function(works) {
			makeWorks(works);
		});
	}, false);

document
	.getElementById('js-textureForm')
	.querySelector('input[type="file"]')
	.addEventListener('change', function() {
		uploadFiles(TEXTURE_IMAGE_PATH, this.files, function(texture) {
			var setSize;

			if (texture.width >= texture.height) {
				var scale = (texture.width / 200);
				var imgMargin = 'style="margin-top: ' + (100 - ((texture.height / scale) / 2)) + 'px;"';

				setSize = 'width="200" ' + imgMargin;
			} else {
				setSize = 'height="200"';
			}

			$textureWindow
				.find('.js-textureList')
				.append('<li class="texture_list fl js-textureItem"><img src="' + texture.fileDir + '" ' + setSize + ' class=" js-textureImage"></li>');
		});
	}, false);

var keyControler = {
	camera: camera,

	unitRotation: 0.03,

	unitPosition: 0.7,

	animationId: {},

	downCode: {},

	rotationState: 0,

	moveCamera: function(e) {
		var a = false;
		var actionList = [37, 38, 39, 40, 87, 68, 83, 65, 13, 16];

		_.forEach(actionList, function(code) {
			if (code === e.keyCode) {
				a = true;
			}
		});

		if (!a) {
			return;
		}

		switch(e.keyCode) {
			case 38: 
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.goFront(e.keyCode);
				}

				break;
			case 40:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.goBack(e.keyCode);
				}

				break;
			case 37:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.goLeft(e.keyCode);
				}

				break;
			case 39:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.goRight(e.keyCode);
				}

				break;
			case 87: 
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.rorateUp(e.keyCode);
				}

				break;
			case 68:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.rorateRight(e.keyCode);
				}

				break;
			case 83:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.rorateDown(e.keyCode);
				}

				break;
			case 65:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.rorateLeft(e.keyCode);
				}

				break;
			case 13:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.up(e.keyCode);
				}

				break;
			case 16:
				if (keyControler.downCode[e.keyCode] !== e.keyCode) {
					keyControler.downCode[e.keyCode] = e.keyCode;
					keyControler.down(e.keyCode);
				}

				break;
		}
	},

	// 前進 38
	goFront: function() {
		keyControler.animationId[keyControler.downCode[38]] = requestAnimationFrame(keyControler.goFront);

		keyControler.camera.position.z -= keyControler.unitPosition;
	},
	// 後退 40
	goBack: function() {
		keyControler.animationId[keyControler.downCode[40]] = requestAnimationFrame(keyControler.goBack);

		keyControler.camera.position.z += keyControler.unitPosition;
	},
	// 左 37
	goLeft: function() {
		keyControler.animationId[keyControler.downCode[37]] = requestAnimationFrame(keyControler.goLeft);

		keyControler.camera.position.x -= keyControler.unitPosition;
	},
	// 右 39
	goRight: function() {
		keyControler.animationId[keyControler.downCode[39]] = requestAnimationFrame(keyControler.goRight);

		keyControler.camera.position.x += keyControler.unitPosition;
	},
	// 上を向く
	rorateUp: function() {
		if (Math.PI / 2 <= keyControler.camera.rotation.x) {
			return;
		}

		keyControler.animationId[keyControler.downCode[87]] = requestAnimationFrame(keyControler.rorateUp);
		keyControler.camera.rotation.x += keyControler.unitRotation;
	},
	// 下を向く
	rorateDown: function() {
		if (- Math.PI / 2 >= keyControler.camera.rotation.x) {
			return;
		}

		keyControler.animationId[keyControler.downCode[83]] = requestAnimationFrame(keyControler.rorateDown);
		keyControler.camera.rotation.x -= keyControler.unitRotation;
	},
	// 左を向く
	rorateLeft: function() {
		keyControler.animationId[keyControler.downCode[65]] = requestAnimationFrame(keyControler.rorateLeft);
		keyControler.camera.rotation.y += keyControler.unitRotation;
	},
	// 右を向く
	rorateRight: function() {
		keyControler.animationId[keyControler.downCode[68]] = requestAnimationFrame(keyControler.rorateRight);
		keyControler.camera.rotation.y -= keyControler.unitRotation;
	},
	// 上
	up: function() {
		keyControler.animationId[keyControler.downCode[13]] = requestAnimationFrame(keyControler.up);
		keyControler.camera.position.y += keyControler.unitPosition;
	},
	// 下
	down: function() {
		keyControler.animationId[keyControler.downCode[16]] = requestAnimationFrame(keyControler.down);
		keyControler.camera.position.y -= keyControler.unitPosition;
	},
	// 止まる
	stopCamera: function(e) {
		cancelAnimationFrame(keyControler.animationId[e.keyCode]);

		delete keyControler.animationId[e.keyCode];
		delete keyControler.downCode[e.keyCode];
	}
};

var objControler = {
	modelObject: null,

	lineObject: null,
	lineObjectX: null,
	lineObjectY: null,
	lineObjectZ: null,

	unitRotation: 0.03,

	unitPosition: 1,

	animationId: {},

	downCode: {},

	rotationState: 0,

	init: function(model, line) {
		this.modelObject = model;
		this.lineObject = line;
	},

	moveModelObject: function(e) {
		var a = false;
		var actionList = [37, 38, 39, 40, 87, 68, 83, 65, 13, 16];

		_.forEach(actionList, function(code) {
			if (code === e.keyCode) {
				a = true;
			}
		});

		if (!a) {
			return;
		}

		switch(e.keyCode) {
			case 38: 
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.goFront(e.keyCode);
				}

				break;
			case 40:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.goBack(e.keyCode);
				}

				break;
			case 37:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.goLeft(e.keyCode);
				}

				break;
			case 39:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.goRight(e.keyCode);
				}

				break;
			case 87: 
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.rorateUp(e.keyCode);
				}

				break;
			case 68:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.rorateRight(e.keyCode);
				}

				break;
			case 83:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.rorateDown(e.keyCode);
				}

				break;
			case 65:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.rorateLeft(e.keyCode);
				}

				break;
			case 13:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.up(e.keyCode);
				}

				break;
			case 16:
				if (objControler.downCode[e.keyCode] !== e.keyCode) {
					objControler.downCode[e.keyCode] = e.keyCode;
					objControler.down(e.keyCode);
				}

				break;
		}
	},

	// 前進 38
	goFront: function() {
		objControler.animationId[objControler.downCode[38]] = requestAnimationFrame(objControler.goFront);
		objControler.modelObject.position.z = objControler.lineObject.geometry.vertices[1].z -= objControler.unitPosition;

		objControler.lineObject.geometry.verticesNeedUpdate = true;
	},
	// 後退 40
	goBack: function() {
		objControler.animationId[objControler.downCode[40]] = requestAnimationFrame(objControler.goBack);
		objControler.modelObject.position.z = objControler.lineObject.geometry.vertices[1].z += objControler.unitPosition;

		objControler.lineObject.geometry.verticesNeedUpdate = true;
	},
	// 左 37
	goLeft: function() {
		objControler.animationId[objControler.downCode[37]] = requestAnimationFrame(objControler.goLeft);
		objControler.modelObject.position.x = objControler.lineObject.geometry.vertices[1].x -= objControler.unitPosition;

		objControler.lineObject.geometry.verticesNeedUpdate = true;
	},
	// 右 39
	goRight: function() {
		objControler.animationId[objControler.downCode[39]] = requestAnimationFrame(objControler.goRight);
		objControler.modelObject.position.x = objControler.lineObject.geometry.vertices[1].x += objControler.unitPosition;

		objControler.lineObject.geometry.verticesNeedUpdate = true;
	},
	// 上を向く
	rorateUp: function() {
		objControler.animationId[objControler.downCode[87]] = requestAnimationFrame(objControler.rorateUp);
		objControler.modelObject.rotation.x += objControler.unitRotation;
	},
	// 下を向く
	rorateDown: function() {
		objControler.animationId[objControler.downCode[83]] = requestAnimationFrame(objControler.rorateDown);
		objControler.modelObject.rotation.x -= objControler.unitRotation;
	},
	// 左を向く
	rorateLeft: function() {
		objControler.animationId[objControler.downCode[65]] = requestAnimationFrame(objControler.rorateLeft);
		objControler.modelObject.rotation.y += objControler.unitRotation;
	},
	// 右を向く
	rorateRight: function() {
		objControler.animationId[objControler.downCode[68]] = requestAnimationFrame(objControler.rorateRight);
		objControler.modelObject.rotation.y -= objControler.unitRotation;
	},
	// 上
	up: function() {
		objControler.animationId[objControler.downCode[13]] = requestAnimationFrame(objControler.up);
		objControler.modelObject.position.y = objControler.lineObject.geometry.vertices[1].y += objControler.unitPosition;

		objControler.lineObject.geometry.verticesNeedUpdate = true;
	},
	// 下
	down: function() {
		objControler.animationId[objControler.downCode[16]] = requestAnimationFrame(objControler.down);
		objControler.modelObject.position.y = objControler.lineObject.geometry.vertices[1].y -= objControler.unitPosition;

		objControler.lineObject.geometry.verticesNeedUpdate = true;
	},
	// 止まる
	stopModelObject: function(e) {
		cancelAnimationFrame(objControler.animationId[e.keyCode]);

		delete objControler.animationId[e.keyCode];
		delete objControler.downCode[e.keyCode];
	}
};

// オブジェクト選択 / 解除
$managerView.on('click', '.js-infoName', function() {
	var $this = $(this);

	if ($this.hasClass('is-selected')) {
		$this.removeClass('is-selected');
		objectManager.unselectObject();
		objControler.modelObject = null;

		$doc
			.off('keydown')
			.off('keyup');
		$doc
			.on('keydown', keyControler.moveCamera)
			.on('keyup', keyControler.stopCamera);

		return;
	}

	_.forEach($managerView.find('.js-infoName'), function(item) {
		var $item = $(item);

		if ($item.hasClass('is-selected')) {
				$item.removeClass('is-selected');
				objectManager.unselectObject();
				objControler.modelObject = null;

				$doc
					.off('keydown')
					.off('keyup');
				$doc
					.on('keydown', keyControler.moveCamera)
					.on('keyup', keyControler.stopCamera);
		}
	});

	$this.addClass('is-selected');
	objectManager.selectObject($(this).text());
	objControler.init(objectManager.getObject($(this).text()), objectManager.line.mesh);

	$doc
		.off('keydown')
		.off('keyup');
	$doc
		.on('keydown', objControler.moveModelObject)
		.on('keyup', objControler.stopModelObject);
});

var saveWallsMaterial = function (targetList) {
	targetList.forEach(function( wall ) {
		wall.matBackup = {
			color: wall.material.color,
			map: wall.material.map
		};
	});
}

var projector = new THREE.Projector();
var mouseVector = new THREE.Vector3();
//マウスのグローバル変数
var mouse = { x: 0, y: 0 };  
//オブジェクト格納グローバル変数
var targetList = room.children;
var selectWall;

saveWallsMaterial(targetList);

// 壁ホバー
$win.on('mousemove', function (e) {
	if (e.target !== renderer.domElement) { 
		return;
	}

	mouseVector.x = 2 * (e.clientX / $win.width()) - 1;
	mouseVector.y = 1 - 2 * ( e.clientY / $win.height());

	var raycaster = projector.pickingRay( mouseVector.clone(), camera ),
		intersects = raycaster.intersectObjects( targetList );

	targetList.forEach(function( wall ) {
		// console.log(wall);
		wall.material.color = wall.matBackup.color;
	});

	// for( var i = 0; i < intersects.length; i++ ) {
		var intersection = intersects[ 0 ],
			obj = intersection.object;

		obj.material.color = new THREE.Color(0xffdddf);
	// }
});

// 壁選択
$win.on('mousedown', function (ev) {
	if (ev.target === renderer.domElement) { 
		//マウス座標2D変換
		var rect = ev.target.getBoundingClientRect();
		mouse.x =  ev.clientX - rect.left;
		mouse.y =  ev.clientY - rect.top;
		
		//マウス座標3D変換 width（横）やheight（縦）は画面サイズ
		mouse.x =  (mouse.x / $win.width()) * 2 - 1;
		mouse.y = -(mouse.y / $win.height()) * 2 + 1;
		
		// マウスベクトル
		var vector = new THREE.Vector3( mouse.x, mouse.y ,1);

		// vector はスクリーン座標系なので, オブジェクトの座標系に変換
		projector.unprojectVector(vector, camera);

		// 始点, 向きベクトルを渡してレイを作成
		var ray = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

		 // クリック判定
		var obj = ray.intersectObjects(targetList);

		 // クリックしていたら、alertを表示  
		if ( obj.length > 0 ){
			selectWall = obj[0].object;
			$textureWindow.fadeIn();
		}
	}
});

$header
	.on('click', '.js-globalMenuList', function() {
		var $selfPanel = $('#' + $(this).attr('rel'));

		if ($selfPanel.css('top') !== '0px') {
			$selfPanel.animate({top: '0px'}, 150);

			return;
		}

		_.forEach($('#js-settingArea').find('.js-objSettingArea'), function(item) {
			var $item = $(item);

			if ($item.css('top') !== '0px') {
				$item.animate({top: '0px'}, 150);
			}
		});

		$selfPanel.animate({top: '50px'}, 300);
	});

$('input')
	.on('focus', function(){
		$doc.off('keydown');
	})
	.on('blur', function() {
		$doc.on('keydown', keyControler.moveCamera);
	});

$doc.on('keydown', function(e) {
	if (e.keyCode === 27) {
		$textureWindow.fadeOut();
	}
});

// テクスチャ管理
$textureWindow
	.on('click', '.js-textureWindowClose', function() {
		$textureWindow.fadeOut();
	})
	.on('click', '.js-textureItem', function() {
		$(this).toggleClass('is-textureSelected');
	})
	.on('click', '#js-setTexture', function() {
		var $selectImg = $textureWindow.find('.is-textureSelected');

		// if (!$selectImg) {
		// 	alert('テクスチャを選択してください');
		// }

		// selectWall.material.map.needsUpdate = true;
		// var imgPath = $selectImg.find('.js-textureImage').attr('src');
		var imgPath = '/images/check.jpg';
		var newMat = new THREE.MeshPhongMaterial({
				map: THREE.ImageUtils.loadTexture(imgPath),
				side: THREE.DoubleSide,
				shininess: 0,
				ambient:0x000000
			});
		newMat.needsUpdate = true;
		selectWall.material.map = THREE.ImageUtils.loadTexture(imgPath);
		// console.log(selectWall.material);
		// console.log(selectWall.material);
		// selectWall.material = newMat;
		selectWall.material.needsUpdate = true;
		$textureWindow.fadeOut(function() {
			$selectImg.removeClass('is-textureSelected');
		});
	});

// カメラ操作
// $doc
// 	.on('keydown', keyControler.moveCamera)
// 	.on('keyup', keyControler.stopCamera);
