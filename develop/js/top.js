'use strict';

var $ = require('jquery');

var $win = $(window);
var $logo = $('#logo');
var $enter = $('#js-enter');
var $field = $('#field');
var $blackOut = $('#blackOut');
var width = $field.width();
var height = $field.height();
var house, door, cosmo, moveFlagY, moveFlagX;
var loader = new THREE.OBJMTLLoader();

moveFlagX = moveFlagY = 0;

// x=前後, y=上下,  z=左右
var loading = {
  // 家、地面、 空、光で5
  state: 4,

  load: function() {
    var self = this;

    if (self.state === 0) {
      setTimeout(function() {
        $('#js-loading').find('.js-loadingGIF').fadeOut(500, function() {
          $('#js-loading').fadeOut(500);
        });
      }, 2000);


      return;
    }

    setTimeout(function() {
      self.load();
    }, 250);
  }
};

loading.load();
// /* 円周率を角度に変換する */
// var PItoAngle = function (PI) {
//   // 一回転以上している場合があるので、その場合は正しい値に戻す
//   var num = PI;

//   var rotationsNum = Math.abs(Math.floor(num / (Math.PI * 2)));

//   // もし負の値ならば正の値に変換する
//   if (PI < 0) {
//     num = PI + (Math.PI * 2) * (rotationsNum + 1);
//   }

//   // 一回転以上の値を通常の角度の値に戻す
//   var overNum = num - (Math.PI * 2) * rotationsNum;
//   var unit = 360 / (Math.PI * 2);

//   return unit * overNum;
// }

/** シーンの生成 **/
var scene = new THREE.Scene();
scene.fog = new THREE.FogExp2(0xccccff, 0.008);

/** カメラの生成 **/
// 視野角・アスペクト比・最小描画範囲・最大描画範囲
var camera = new THREE.PerspectiveCamera(45, width / height, 1, 1000);
// x軸, y軸, z軸
camera.position.set(0, 13, 85);
camera.rotation.x += 0.15;
// カメラの表示範囲
camera.aspect = 600 / 400;
/** ライトの作成 **/
function setLight(){
  // スポットライト
  var spotLight = new THREE.SpotLight(0xffffff, 2, 130, 0.5, 19);
  spotLight.target.position.set(0, 0, 40);
  spotLight.position.set(0, 60, 90);
  spotLight.castShadow = true;
  spotLight.shadowBias = -0.0001;
  spotLight.shadowDarkness = 0.4;

  // スポットライト2
  var spotLight2 = new THREE.SpotLight(0xffffff, 0.7, 130);
  spotLight2.target.position.set(0, 0, 40);
  spotLight2.position.set(0, 60, 40);
  spotLight2.castShadow = true;
  spotLight2.shadowBias = -0.0001;
  spotLight2.shadowDarkness = 0.7;

  // //環境光
  // var ambientLight = new THREE.AmbientLight(0x444433);

  // 空の光
  var hemiLight = new THREE.HemisphereLight( 0x7777ff, 0xcccccc, 0.3 ); 

  scene.add(spotLight);
  scene.add(spotLight2);
  // scene.add(ambientLight);
  scene.add(hemiLight);

  loading.state--;
}

function setPlane(){
  /** でこぼこした地面を作る **/
  // var simplexNoise = new SimplexNoise();
  // var map1 = THREE.ImageUtils.loadTexture( '/images/suna.jpg' );
  var pGeometry = new THREE.PlaneGeometry(190, 190, 300, 300);

  // ジオメトリをでこぼこさす
  // for ( var i = 0; i < pGeometry.vertices.length; i++ ) {
  //     var vertex = pGeometry.vertices[i];
  //     vertex.z = simplexNoise.noise(vertex.x / 100, vertex.y / 100);
  // }

  // // でこぼこの影を作る
  // pGeometry.computeFaceNormals();
  // pGeometry.computeVertexNormals();

  var plane = new THREE.Mesh(
    pGeometry, 
    new THREE.MeshPhongMaterial({
      color: 0xcccccc,
      // map: map1,
      // bumpMap: map1,
      // bumpScale: 5,
      shininess: 10,
      ambient:0xffffff
    })
  );

  plane.position.set(0, 11.5, -10);
  plane.rotation.x = -90 * Math.PI / 180;
  plane.receiveShadow = true; 

  scene.add(plane);

  loading.state--;
}

/** 3Dモデルを読み込む **/
function setHouse(){
  var house_obj_load = '/model/house/house.obj';
  var house_mtl_load = '/model/house/house.mtl';
  var door_obj_load = '/model/house/door.obj';
  var door_mtl_load = '/model/house/door.mtl';

  loader.load(house_obj_load, house_mtl_load, function ( object ) {

    object.position.set(0, 11.7, 35);
    object.receiveShadow = true;
    object.castShadow = true;
    object.rotation.y = Math.PI;
    scene.add(object);

    object.scale.x = 0.9;
    object.scale.y = 0.9;
    object.scale.z = 0.9;

    object.traverse(function(node) {
      if (node instanceof THREE.Mesh) {
        node.castShadow = true;
        node.receiveShadow = true;
        node.ambient = 0xffffff;
      }
    });

    house = object;

    loading.state--;
  });

  loader.load(door_obj_load, door_mtl_load, function ( object ) {

    object.position.set(0, 11.7, 44.5);
    object.receiveShadow = true;
    object.castShadow = true;
    object.rotation.y = Math.PI;
    scene.add(object);

    object.scale.x = 0.9;
    object.scale.y = 0.9;
    object.scale.z = 0.9;

    object.traverse(function(node) {
      if (node instanceof THREE.Mesh) {
        node.castShadow = true;
        node.receiveShadow = true;
        node.ambient = 0xffffff;
      }
    });

    door = object;
  });
}
// var doorGeo = new THREE.BoxGeometry(10, 15, 1);
// var doorMaterial = new THREE.MeshLambertMaterial({color: 0xff0000, side: THREE.DoubleSide, ambient: 0xffffff});

// var door = new THREE.Mesh(doorGeo, doorMaterial);
// // 影をON
// door.castShadow = true;
// door.receiveShadow = true;
// door.position.set(0, 16, 0);
// scene.add(door);

// $('h2').on('click', function() {
//   open();
// });
// function open() {  
//   if (door.rotation.y >= -Math.PI / 2 || door.position.x >= -4 || door.position.z <= +4) {
//     requestAnimationFrame(open);
//   }

//   if (door.rotation.y >= -Math.PI / 2) {
//     door.rotation.y -= 0.15;
//   }

//   if (door.position.x >= -4) {
//     door.position.x -= 0.3;
//   }

//   if (door.position.z <= +5) {
//     door.position.z += 0.35;
//   }
// }

function setCosmo() {
  var cosmoMap = new THREE.ImageUtils.loadTexture('/images/cosmo5.jpg');
  cosmo = new THREE.Mesh(
    new THREE.SphereGeometry(120, 300, 300),
    new THREE.MeshLambertMaterial({
      map: cosmoMap,
      bumpMap: cosmoMap,
      bumpScale: 3,
      side: THREE.DoubleSide,
      ambient: 0xffffff 
    })
  );
  cosmo.scale.y = 0.15;
  cosmo.scale.x = 0.95;
  cosmo.scale.z = 1;

  cosmo.position.set(0, 14, 0);
  scene.add(cosmo);

  loading.state--;
}

// enter
var enterCenter = function($enter, $win) {
  var $enter = $('#js-enter');

  $enter.css('left', ($win.width()/2) - 103.5);
};

/** レンダラーの生成 **/

var renderer;

// WebGLの有無を確認
if (window.WebGLRenderingContext) {
  renderer = new THREE.WebGLRenderer({ antialias: true }); 
} else {
  renderer = new THREE.CanvasRenderer({ antialias: true });
}

// var floorTexture = new THREE.ImageUtils.loadTexture('images/checkerboard.jpg');
// floorTexture.wrapS = floorTexture.wrapT = THREE.RepeatWrapping; 
// floorTexture.repeat.set(10, 10);
var floorMaterial = new THREE.MeshBasicMaterial({
  color: 0x000000, side: THREE.DoubleSide
});
var floorGeometry = new THREE.PlaneGeometry(100, 100, 10, 10);
var floor = new THREE.Mesh(floorGeometry, floorMaterial);
floor.position.y = 11;
floor.position.z = 35;
floor.rotation.x = Math.PI / 2;
scene.add(floor);

// オブジェクトをシーンに追加する
setLight();
setPlane();
setHouse();
setCosmo();
// ENTER中央寄せ
$(function() {
  enterCenter($enter, $win);
});

// 影をON
renderer.shadowMapEnabled = true; 
// 影をソフトシャドウに
renderer.shadowMapSoft = true;
renderer.shadowMapType = THREE.PCFSoftShadowMap;
// canvasサイズ設定
renderer.setSize(width, height);
// canvasサイズ設定
renderer.setClearColor(0x99aacc, 1);
// HTMLに埋め込む
$field
  // .css({
  //   width: width,
  //   height: height
  // })
  .html(renderer.domElement);
// レンダリング
myRender();

function myRender() {
  requestAnimationFrame(myRender);
  renderer.render(scene, camera);
}

var blackOutFlag = 0;

function blackOut($blackOut, callback) {
  if (blackOutFlag === 0) {
    blackOutFlag = 1;

    $blackOut.fadeIn(2000, callback); 
  }
}

function houseMove() {
  var animationId = requestAnimationFrame(houseMove);

if (camera.position.x < 0) {
    camera.position.x += 1;

    return;
  } else if (camera.position.x > 1.1) {
    camera.position.x -= 1;

    return;
  } else if (camera.position.z > 35) {
    camera.position.z -= 2;
    blackOut($blackOut, function() {
      location.href = '/creatRoom';
    });

    return;
  }

  // if (door.position.z <= 47) {
  //   door.position.z += 0.2;

  //   return;
  // } else if (door.position.x <= 15){
  //   door.position.x += 0.5;

  //   return;
  // } else if (camera.position.x < 0) {
  //   camera.position.x += 0.5;

  //   return;
  // } else if (camera.position.x > 6) {
  //   camera.position.x -= 1;

  //   return;
  // } else if (camera.position.z > 35) {
  //   camera.position.z -= 1;
  //   blackOut($blackOut, function() {
  //     location.href = '/edit';
  //   });

  //   return;
  // }

  // 縦横にぐにょんぐにょん
  // if (house.scale.y <= 0.94 && moveFlagY === 0) {
  //   house.scale.y += 0.05;
  //   door.scale.y += 0.05;
  //   return;
  // } else if (house.scale.y >= 0.8) {
  //   moveFlagY = 1;
  //   house.scale.y -= 0.05;
  //   door.scale.y -= 0.05;
  //   console.log(house.scale.y);
  //   return;
  // } else if (house.scale.x <= 0.85 && moveFlagX === 0) {
  //   house.scale.x += 0.05;
  //   door.scale.x += 0.05;
  //   return;
  // } else if (house.scale.x >= 0.8) {
  //   moveFlagX = 1;
  //   house.scale.x -= 0.05;
  //   door.scale.x -= 0.05;
  //   return;
  // }

  cancelAnimationFrame(animationId);
}
$enter.on('click', function() {
  $logo.fadeOut();
  $enter.fadeOut();
  document.removeEventListener('mousemove', moveCamera);
  houseMove();
});

var fogAnimationId;
/* 霧を徐々に薄くする */
function fadeOutFog() {
  if (scene.fog.density >= 0.008) {
    fogAnimationId = requestAnimationFrame(fadeOutFog);
    scene.fog.density -= 0.001;
  } else {
    cancelAnimationFrame(fogAnimationId);
  }
}

/* マウスの動きに合わせてカメラを移動させる */
function moveCamera(e) {
  var winHalfSize = $win.width() / 2;
  var mousePositionX = (e.clientX - winHalfSize) / 60;

  camera.position.x = mousePositionX;
  cosmo.position.x = mousePositionX;
}

// マウスの動きに合わせてカメラを移動させる
document.addEventListener('mousemove', moveCamera);