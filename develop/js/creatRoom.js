'use strict';

var $ = require('jquery');
var _ = require('underscore');

var creatRoom = require('./room');

var SELECTED_CLASS = 'is-selected';
var CHECKED_CLASS = 'is-checked';
var isDown = false;
var roomObject, roomData;

// 関数定義
/**
 * checkSquaresTool 選択状態を見て真偽値を返す
 * @param {Object} $squares 選択ボックスの要素
 */
var checkSquaresTool = function($squares) {
	var selectedList = _.filter($squares, function(item) {return $(item).hasClass(SELECTED_CLASS);});
	var checkedList = [];
	// まず空かどうか調べる
	var result = _.some(_.map($squares, function(box) {
		return $(box).hasClass(SELECTED_CLASS);
	}), _.identity);

	if (!result) {
		return false;
	}

	$(selectedList[0]).addClass(CHECKED_CLASS);

	// 選択が正しい状態かチェックする
	// 全ての選択ボックスが上下左右いずれかに隣接している状態であればtrue
	// していない状態であればfalseを返す
	_.forEach($squares, function() {
		checkedList = _.filter($squares, function(item) {return $(item).hasClass(CHECKED_CLASS);});

		_.forEach(checkedList, function(item) {
		var $item = $(item);
			var itemIndex = $squares.index($item);
			var $up = $squares.eq(itemIndex - 10);
			var $left = $squares.eq(itemIndex - 1);
			var $right = $squares.eq(itemIndex + 1);
			var $down = $squares.eq(itemIndex + 10);
			var upResult = $up.hasClass(SELECTED_CLASS);
			var leftResult = $left.hasClass(SELECTED_CLASS);
			var rightResult = $right.hasClass(SELECTED_CLASS);
			var downResult = $down.hasClass(SELECTED_CLASS);

			if (upResult && !$up.hasClass(CHECKED_CLASS)) {
				$up.addClass(CHECKED_CLASS);
			}

			if (leftResult && !$left.hasClass(CHECKED_CLASS)) {
				$left.addClass(CHECKED_CLASS);
			}

			if (rightResult && !$right.hasClass(CHECKED_CLASS)) {
				$right.addClass(CHECKED_CLASS);
			}

			if (downResult && !$down.hasClass(CHECKED_CLASS)) {
				$down.addClass(CHECKED_CLASS);
			}
		});
	});

	if (checkedList.length !== selectedList.length) {
		return false;
	}

	return true;
};

var getRoomInfo = function($squares, $height) {
	var roomInfo = {
		userId: USER_ID,
		height: Number($height.val()),
		wallInfo: null
	};
	var wallInfo = [];

	_.forEach($squares, function(item, i) {
		var $item = $(item);
		var itemInfo = [];
		var nowPositionState = $item.hasClass(SELECTED_CLASS);

		itemInfo.push(nowPositionState);

		// 最上段が選択されていれば上は壁確定
		if (i >= 10 && !nowPositionState) {
			itemInfo.push($squares.eq(i - 10).hasClass(SELECTED_CLASS));
		} else if (i < 10 && nowPositionState) {
			itemInfo.push(true);
		} else {
			itemInfo.push(false);
		}

		// 右の段であれば右をカウントしない
		if (i % 10 !== 9 && i !== 9 && !nowPositionState) {
			itemInfo.push($squares.eq(i + 1).hasClass(SELECTED_CLASS));
		} else if (i % 10 === 9 && nowPositionState || i === 9 && nowPositionState) {
			itemInfo.push(true);
		} else {
			itemInfo.push(false);
		}

		// 最下段であれば下をカウントしない
		if (i < 90 && !nowPositionState) {
			itemInfo.push($squares.eq(i + 10).hasClass(SELECTED_CLASS));
		} else if (i >= 90 && nowPositionState) {
			itemInfo.push(true);
		} else {
			itemInfo.push(false);
		}

		// 左の段であれば左をカウントしない
		if (i % 10 !== 0 && i !== 0 && !nowPositionState) {
			itemInfo.push($squares.eq(i - 1).hasClass(SELECTED_CLASS));
		} else if (i % 10 === 0 && nowPositionState || i === 0 && nowPositionState) {
			itemInfo.push(true);
		} else {
			itemInfo.push(false);
		}

		wallInfo.push(itemInfo);
	});
	roomInfo.wallInfo = wallInfo;

	return roomInfo;
};

var creatField = function() {
	/** シーンの生成 **/
	var scene = new THREE.Scene();

	/** ライトの作成 **/
	// 空の光
	var hemiLight = new THREE.HemisphereLight(0xffffff, 0xcccccc, 0.7); 

	// スポットライト
	var spotLight = new THREE.SpotLight(0xccffff, 2, 130, 1, 19);
	spotLight.target.position.set(0, 0, 40);
	spotLight.position.set(0, 60, 90);
	spotLight.castShadow = true;
	spotLight.shadowBias = -0.0001;
	spotLight.shadowDarkness = 0.4;
	spotLight.shadowMapWidth = spotLight.shadowMapHeight = 1024;
	spotLight.shadowCameraNear = 1;
	spotLight.shadowCameraFar = 600;
	spotLight.shadowCameraFov = 45;

	scene.add(hemiLight);
	scene.add(spotLight);

	return scene;
};

// 要素の取得
var $window = $(window);
var $squaresTool = $('#js-squaresTool');
var $domeTool = $('#js-domeTool');
var $domeToolRoom = $domeTool.find('.js-domeToolRoom');
var $domeToolBox = $domeTool.find('.js-roomToolBox');
var $squares = $squaresTool.find('.js-squaresToolBox');
var $roomView = $('#js-roomView');
var $creat = $('#js-creatWindow');
var $result = $('#js-resultWindow');
var $roomHeight = $creat.find('.js-roomHeight');
var $domeOptions = $('#js-domeOptions');
var $domeOptionMenu = $domeOptions.parent('.js-roomOptions');
var $roomRadius = $domeOptionMenu.find('.js-roomRadius');
var $roomInfoForm = $('#js-roomInfoForm');
var width = $window.width() * 0.75;
var height = $window.height() * 0.75;
var scene = creatField();
var reqAnimation;
/** レンダラーの生成 **/
var renderer;

$('#js-backLayer').fadeOut();

// WebGLの有無を確認
if (window.WebGLRenderingContext) {
	renderer = new THREE.WebGLRenderer({ antialias: true }); 
} else {
	renderer = new THREE.CanvasRenderer({ antialias: true });
}

renderer.setSize(width, height);

/** カメラの生成 **/
// 視野角・アスペクト比・最小描画範囲・最大描画範囲
var camera = new THREE.PerspectiveCamera(50, width / height, 1, 500);
// x軸, y軸, z軸
camera.position.set(0, 30, 30);
// カメラの表示範囲
camera.aspect = width / height;

// 影をON
renderer.shadowMapEnabled = true; 
renderer.shadowMapCullFace = THREE.CullFaceBack;
// 影をソフトシャドウに
renderer.shadowMapSoft = true;
renderer.shadowMapType = THREE.PCFSoftShadowMap;
// canvasサイズ設定
renderer.setSize(width, height);
// canvasサイズ設定
renderer.setClearColor(0xffffff, 1);
// HTMLに埋め込む
$roomView
	.width(width)
	.height(height)
	.html(renderer.domElement);

// レンダリング
function myRender() {
	reqAnimation = requestAnimationFrame(myRender);
	camera.position.x = Math.sin(new Date().getTime() / 4000) * 19;
	camera.position.z = Math.cos(new Date().getTime() / 4000) * 19;
	camera.lookAt(new THREE.Vector3(0, 0, 0));
	renderer.render(scene, camera);
}

var $slider = $('#js-configSlider');
var $sliderItem = $slider.find('.js-configSliderItem');
var $sliderNext = $slider.find('.js-configSliderNext');
var itemWidth = $sliderItem.width();
var clickFlg = false;

$slider
	.width(itemWidth * $sliderItem.length)
	.on('click', '.js-configSliderNext', function () {
		if (clickFlg) {
			return;
		}

		clickFlg = true;

		$slider.animate({
			left: Number($slider.css('left').replace('px', '')) - itemWidth
		});

		clickFlg = false;
	})
	.on('click', '.js-configSliderPrev', function () {
		if (clickFlg) {
			return;
		}

		clickFlg = true;

		$slider.animate({
			left: Number($slider.css('left').replace('px', '')) + itemWidth
		});

		clickFlg = false;
	});


// ドラッグ状態をisDownで判別する
$(document)
	.on('mousedown', function() {
		isDown = true;
	})
	.on('mouseup',function() {
		isDown = false;
	});

$domeToolRoom.css({
	marginTop: ($domeTool.height() / 2) - ($domeToolRoom.height() / 2),
	marginLeft: ($domeTool.width() / 2) - ($domeToolRoom.width() / 2)
});

$squares
	.on('mousedown', function() {
		$(this).toggleClass(SELECTED_CLASS);
	})
	.on('mouseenter', function() {
		if (isDown) {
			$(this).toggleClass(SELECTED_CLASS);
		}
	});

$domeOptions.on('click', function () {
	$domeOptionMenu.toggleClass('is-disabled');
	$roomRadius.toggleClass('is-disabled');

	if (!$domeOptionMenu.hasClass('is-disabled')) {
		$squaresTool.hide();
		$domeTool.show();

		$roomRadius.removeAttr("disabled");
	} else {
		$squaresTool.show();
		$domeTool.hide();

		$roomRadius.attr("disabled", "disabled");
	}
});

$roomRadius.on('change', function () {
	var $this = $(this);
	var val = Number($this.val());

	if (val >= 10) {
		$this.val(10);
	}

	$domeToolRoom
		.height(val * 50)
		.width(val * 50)
		.css({
			marginTop: ($domeTool.height() / 2) - ($domeToolRoom.height() / 2),
			marginLeft: ($domeTool.width() / 2) - ($domeToolRoom.width() / 2)
		});
});

// 平面図作成シーン
$('#js-allClear').on('click', function() {
	_.forEach($squares, function(item) {
		$(item).removeClass(SELECTED_CLASS);
		$(item).removeClass(CHECKED_CLASS);
	});
});
$('#js-complete').on('click', function() {
	var isDome = $domeOptionMenu.find('input[type="checkbox"]')[0].checked;

	console.log(isDome);

	if (isDome) {
		roomData = {
			userId: USER_ID,
			height: Number($roomHeight.val()),
			wallInfo: [Number($roomRadius.val()) * 1.4]
		};

		roomObject = creatRoom.getRoom(roomData);
		roomObject.rotation.x = Math.PI / 2;
		roomObject.rotation.y = Math.PI;
	} else if (checkSquaresTool($squares)) {
		roomData = getRoomInfo($squares, $roomHeight);
		roomObject = creatRoom.getRoom(roomData);

		roomObject.rotation.x = Math.PI / 90;
	} else {
		alert('アカン！');
		return false;
	}

		scene.add(roomObject);
		myRender();
		$roomView.fadeIn(400);

	_.forEach($squares, function(item) {
		$(item).removeClass(CHECKED_CLASS);
	});
});

// 部屋生成のシーン
$('#js-ok').on('click', function() {
	$.map(roomData, function(value, key) {
		if (typeof value === 'object') {
			var originalKeyName = 'wall';

			if (typeof value[0] === 'number') {
				$roomInfoForm.append('<input type="hidden" name="' + key + '[0]" value="' + value[0] + '">');
			} else {
				_.forEach(value, function (item, i) {
					_.forEach(item, function (item2) {
						$roomInfoForm.append('<input type="hidden" name="' + key + '[' + i + ']" value="' + item2 + '">');
					});
				});
			}

			return;
		}

		$roomInfoForm.append('<input type="hidden" name="' + key + '" value="' + value + '">');
	});
	$roomInfoForm.submit();
});
$('#js-return').on('click', function() {
	cancelAnimationFrame(reqAnimation);

	scene = creatField();
	roomObject = null;

	$roomView.fadeOut(200);
});