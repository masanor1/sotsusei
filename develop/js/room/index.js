'use strict';

var _ = require('underscore');

var room = {
	WALL_SIZE: 2,

	creatPlane: function(options) {
		var self = this;
		var roomHeight = options.height || self.WALL_SIZE;

		var plane = new THREE.Mesh(new THREE.PlaneGeometry(self.WALL_SIZE, roomHeight, 15, 15), new THREE.MeshPhongMaterial({
				color: 0xffffff,
				side: THREE.DoubleSide,
				shininess: 0,
				bumpScale: 25,
				ambient:0xffffff
			}));

		plane.position.set(options.position.x, options.position.y, options.position.z);

		if (options.rotate) {
			plane.rotation[options.rotate] = 90 * Math.PI / 180;
		}

		options.geometry.add(plane);

		// plane.updateMatrix();
		// geometry.merge(plane.geometry, plane.matrix);
	},

	getRoom: function(roomInfo, ceiling) {
		var self = this;
		var wallInfo = roomInfo.wallInfo;
		var geometry = new THREE.Object3D();
		var room;

		if (typeof wallInfo[0] === 'number') {
			room = new THREE.Mesh(new THREE.SphereGeometry(wallInfo, 50, 50, 0, Math.PI), new THREE.MeshPhongMaterial({
				color: 0xffffff,
				side: THREE.DoubleSide,
				shininess: 0,
				ambient:0xffffff
			}));

			return room;
		}

		_.forEach(wallInfo, function(info, i) {
			if (!_.some(info, _.identity)) {
				return;
			}

			var unit = self.WALL_SIZE * i;
			var position = {
				x: unit - 9,
				y: roomInfo.height / 2,
				z: (Number(i.toString().charAt(0)) * self.WALL_SIZE) - 9
			};

			if (i < 10) {
				position.z = -9;
			}

			if (i >= 10) {
				position.x = (Number(i.toString().charAt(1)) * self.WALL_SIZE) - 9;
			}

			_.forEach(info, function(item, j) {
				var originalPosition = _.clone(position);

				if (!item) {
					return;
				}

				switch (j) {
						// 床
					case 0:
						originalPosition.y = 0;

						self.creatPlane({
							geometry: geometry,
							position: originalPosition,
							rotate: 'x'
						});


						if (ceiling) {
							originalPosition.y = roomInfo.height;

							self.creatPlane({
								geometry: geometry,
								position: originalPosition,
								rotate: 'x'
							});

						}
						break;
						// 上
					case 1:
						originalPosition.z -= self.WALL_SIZE / 2;

						self.creatPlane({
							geometry: geometry,
							position: originalPosition,
							height: roomInfo.height
						});

						break;
						// 右
					case 2:
						originalPosition.x += self.WALL_SIZE / 2;

						self.creatPlane({
							geometry: geometry,
							position: originalPosition,
							rotate: 'y',
							height: roomInfo.height
						});

						break;
						// 下
					case 3:
						originalPosition.z += self.WALL_SIZE / 2;

						self.creatPlane({
							geometry: geometry,
							position: originalPosition,
							height: roomInfo.height
						});

						break;
						// 左
					case 4:
						originalPosition.x -= self.WALL_SIZE / 2;

						self.creatPlane({
							geometry: geometry,
							position: originalPosition,
							rotate: 'y',
							height: roomInfo.height
						});

						break;
				}
			});
		});

		// room = new THREE.Mesh(geometry, material);

		return geometry;
	}
};

module.exports = room;